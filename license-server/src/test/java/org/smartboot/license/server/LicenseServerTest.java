/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseTest.java
 * Date: 2020-03-26
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.server;

import java.io.File;
import java.util.Base64;
import java.util.Date;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/26
 */
public class LicenseServerTest {
    public static void main(String[] args) throws Exception {
        LicenseServer server = new LicenseServer(new File("source.txt"), new File("license.txt"));
        String content = "HelloWorldHelloWorldHelloWorldHelloWorldHelloWorldHelloWorldHelloWorldHelloWorld";
        byte[] privateKey = Base64.getDecoder().decode("MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKgpKIl0qd3K93fJTVRr5jOJ0XoCroImowTarjvtGXa4yd1e3r6F5p3XlKpyhdWDzvtSd+Rr6HSFCsi0XbOJIFdjqbnM1MLkNFUrn6aRlOqrYQdQxfZGI0cgufoQCVT5jeTKxZeSkLcM/Oectc23C2CK8o96cbw8HVDB9HNc4wgpAgMBAAECgYB6qjtVuXJHxcyq5n6wPF0Z70iM1fPyILj9y38ZW6Udg0JNZuzY0dgd/6oEg7G4xINxtQCb8VMJ3Bq3uUo4sqiRp8XHNE5EcFHnUWhOGN82MemZ14MnLj1ev22Fbvs2NjvGhGxYWTbAhJn/Fzd/XiEd4jXTwXKtlfI23s2BHRE/AQJBANeu+46/Rbaa4kLsrrVxK45bjjKK1g8lWspYuCPQGWxDgN5OqEkYPLtZdqmimkCoOHfCLyR1jllQ0JjCV2S6VXECQQDHmBap1vBZjRzpaHC3JrJfir/EBAT40VJYb03qStVZjQYMxZsb78x/9MuwPeNEsMg+843/DNElJecnMOOGXSI5AkBHC+jpeLAqGpL1oKlcM085sYcBtyIUEyX5CYh+o4n8U7AbiqV8jnNS84FXF6zgJ4mSSXDl/aNKYIiWCU5oa8jBAkEAjpQ2nFzOlSoY3SOeYTdjdJzJIF0+r//XsxTPs+6BOddj2vvOMca7byyP1PZgw1EFuvKBHREq1j6dQYHLBtDBAQJAcpOPJq/X4U4FL/NAJ0EW3LM3FLiNFO96xHQVAUBgqHzM5e+nlRhSo9SNg3TF6QoKMpiH0zkt21gzXoO5bBKtLg==");
        server.createLicense(content.getBytes(), new Date(System.currentTimeMillis() + 60 * 1000), privateKey);
    }
}
